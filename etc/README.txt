UUIDGenerator - A simple utility to generate UUIDs
Sean Turley <spt5007@arl.psu.edu>

About
=====
UUIDGenerator.exe is a command line utility to generate UUID strings. It uses
Boost to do the actual generation and will produce as many UUIDs as you specify.

Basic Usage
===========
Simply double click on OneUUID.bat to generate a single UUID in a console
window. You can also use TenUUIDsToFile.bat which generates ten UUIDs to a file 
called UUIDs.txt. These batch files can be modified to suit your purpose

Advanced Usage
==============
UUIDGenerator.exe may be used directly on the command line. Call it with no
arguments for usage.
