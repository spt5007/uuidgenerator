#include <boost/uuid/uuid.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/lexical_cast.hpp>

#include <string>
#include <iostream>
#include <vector>
#include <exception>

#include <stdint.h>

void GenerateUUIDStrings(uint32_t num_ids, std::vector<std::string> &uuids) {
	boost::uuids::random_generator gen;

	for (uint32_t i = 0; i < num_ids; i++) {
		boost::uuids::uuid UUID = gen(); 
		std::string uuidString = boost::uuids::to_string(UUID);
		uuids.push_back(uuidString);
	}
}

void PrintUUIDStrings(std::vector<std::string> uuids) {
	for (uint32_t i = 0; i < uuids.size(); i++) {
		std::cout << uuids.at(i) << std::endl;
	}
}

uint32_t GetRequestedUUIDCount(int argc, char **argv) {
	uint32_t numUUIDs;

	if (argc > 1) {
		try {
			numUUIDs = boost::lexical_cast<uint32_t, char *>(argv[1]);
		}
		catch (boost::bad_lexical_cast) {
			std::cout << "Problem parsing argument \"" << argv[1] << "\"" << std::endl;
			numUUIDs = 1;
			std::cout << "I'll just assume you meant " << numUUIDs << ", instead." << std::endl;
		}
		catch (std::exception &e) {
			throw e;
		}
	}

	return numUUIDs;
}

void PrintUsage(int argc, char **argv) {
	std::cout << argv[0] << " <number of UUIDs to generate>" << std::endl;
}

int main (int argc, char **argv) {

	if (argc < 2) {
		PrintUsage(argc, argv);
		return 1;
	}

	std::vector<std::string> uuids;
	uint32_t numUUIDs;

	numUUIDs = GetRequestedUUIDCount(argc, argv);

	std::cout << "Generating " << numUUIDs << " UUIDs" << std::endl;

	GenerateUUIDStrings(numUUIDs, uuids);
	PrintUUIDStrings(uuids);

	return 0;
}